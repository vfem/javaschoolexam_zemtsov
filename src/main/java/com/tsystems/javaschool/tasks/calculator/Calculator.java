package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here

        //Ограничение на вводимые данные
        if (statement == null || statement.equals("")) return null;

        List<Integer> list = new ArrayList<>();
        list.add(48);//0
        list.add(49);//1
        list.add(50);//2
        list.add(51);//3
        list.add(52);//4
        list.add(53);//5
        list.add(54);//6
        list.add(55);//7
        list.add(56);//8
        list.add(57);//9
        list.add(40);//(
        list.add(41);//)
        list.add(46);//.
        list.add(42);//*
        list.add(43);//+
        list.add(45);//-
        list.add(47);// /


        int count1 = 0;
        int count2 = 0;
        for (int i = 0; i < statement.length(); i++){
            if (!list.contains((int) statement.charAt(i))) return null;
            if (statement.charAt(i) == '('){
                count1++;
            }
            if (statement.charAt(i) == ')'){
                count2++;
            }
            if (i + 1 == statement.length() && count1 != count2) return null;
            if (i != 0 && statement.charAt(i) == statement.charAt(i - 1) && (statement.charAt(i) == '.' || statement.charAt(i) == '*' || statement.charAt(i) == '/'
                    || statement.charAt(i) == '-' || statement.charAt(i) == '+')) return null;

        }

        //Проверяю есть ли скобки
        Pattern globalBr = Pattern.compile("\\(.+\\)");
        Matcher matcherGlobal = globalBr.matcher(statement);
        //Обрабатываю скобки рекурсивно
        while (matcherGlobal.find()){
            Pattern patternBr = Pattern.compile("\\([0-9*\\/\\-+]+\\)");
            Matcher matcherBr = patternBr.matcher(statement);
            while (matcherBr.find()){
                String found = matcherBr.group();
                statement = statement.replace(found, evaluate(found.substring(1, found.length() - 1)));
            }
        }

        //Паттерны для математических выражений
        Pattern patternMulti = Pattern.compile("\\d+\\.?\\d*\\*\\-?\\d+\\.?\\d*|^\\-\\d+\\.?\\d*\\*\\-?\\d+\\.?\\d*");
        Pattern patternDiv = Pattern.compile("\\d+\\.?\\d*\\/\\-?\\d+\\.?\\d*|^\\-\\d+\\.?\\d*\\/\\-?\\d+\\.?\\d*");
        Pattern patternSum = Pattern.compile("\\d+\\.?\\d*\\+\\d+\\.?\\d*|^\\-\\d+\\.?\\d*\\+\\d+\\.?\\d*");
        Pattern patternSub = Pattern.compile("\\d+\\.?\\d*\\-\\d+\\.?\\d*|^\\-\\d+\\.?\\d*\\-\\d+\\.?\\d*");
        //Обрабатываю матемачитеские операции
        Matcher matcherDiv = patternDiv.matcher(statement);
        while (matcherDiv.find()){
            String found = matcherDiv.group();
            String[] split = found.split("\\/");
            double first = Double.parseDouble(split[0]);
            double second = Double.parseDouble(split[1]);
            double result = first / second;
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) return null;
            statement = statement.replace(found, fmt(result));
            matcherDiv = patternDiv.matcher(statement);
        }

        Matcher matcherMulti = patternMulti.matcher(statement);
        while (matcherMulti.find()){
            String found = matcherMulti.group();
            String[] split = found.split("\\*");
            double first = Double.parseDouble(split[0]);
            double second = Double.parseDouble(split[1]);
            double result = first * second;
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) return null;
            statement = statement.replace(found, fmt(result));
            matcherMulti = patternMulti.matcher(statement);
        }

        Matcher matcherSub = patternSub.matcher(statement);
        while (matcherSub.find()){
            String found = matcherSub.group();
            String[] split = found.split("(?<!^)\\-");
            double first = Double.parseDouble(split[0]);
            double second = Double.parseDouble(split[1]);
            double result = first - second;
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) return null;
            statement = statement.replace(found, fmt(result));
            matcherSub = patternSub.matcher(statement);
        }

        Matcher matcherSum = patternSum.matcher(statement);
        while (matcherSum.find()){
            String found = matcherSum.group();
            String[] split = found.split("\\+");
            double first = Double.parseDouble(split[0]);
            double second = Double.parseDouble(split[1]);
            double result = first + second;
            if (result == Double.POSITIVE_INFINITY || result == Double.NEGATIVE_INFINITY) return null;
            statement = statement.replace(found, fmt(result));
            matcherSum = patternSum.matcher(statement);
        }


        return statement;
    }
    //Метод для форматирования вывода
    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }

}
