package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try{

            List<Integer> list = new ArrayList<>();

            for (Integer integer : inputNumbers){
                if (integer != null){
                    list.add(integer);
                } else {
                    throw new CannotBuildPyramidException();
                }

            }

            //список допустимых значений
            List<Integer> sizes = new ArrayList<>();
            sizes.add(1);
            sizes.add(3);
            sizes.add(6);
            sizes.add(10);
            sizes.add(15);
            sizes.add(21);
            sizes.add(28);
            sizes.add(36);
            sizes.add(45);
            sizes.add(55);
            //расширяю список, если необходимо
            while (list.size() > sizes.get(sizes.size() - 1)) {
                sizes.add(sizes.get(sizes.size() - 1) + sizes.size() + 1);
            }
            //бросаем исключение, если симметричную пирамиду не построить
            if (!sizes.contains(list.size())){
                throw new CannotBuildPyramidException();
            }

            Collections.sort(list);
            //считаем число строк
            int n = list.size();
            int idx = 0;
            int numRows = 0;
            int lastRow = 1;
            while (idx < n){
                numRows++;
                for (int numInRow = 0; numInRow < numRows; numInRow++){
                    idx++;
                }
            }
            //считаем число столбцов
            int buffer = 1;
            for (int i = 1; i < numRows; i++){
                lastRow = buffer + 2;
                buffer = lastRow;
            }

            int[][] output = new int[numRows][lastRow];

            if (n == 1){
                output[0][0] = list.get(0);
                return output;
            }
            //заполняем, если чисел больше одного
            int midPos = output[0].length / 2;
            for (int i = 0; i < output.length; i++){
                int count = 0;
                for (int j = 0; j < output[i].length; j++){
                    if (j >= midPos - i && count <= i){
                        output[i][j] = list.get(0);
                        count++;
                        list.remove(0);
                        j++;
                    }
                }
            }

            return output;
        } catch (Throwable e){
            throw new CannotBuildPyramidException();
        }

    }


}
